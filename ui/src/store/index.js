import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '../router'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        events: [],
        event: {},
        signup: {},
        signin: {},
        user: {},
        offset: 0,
        limit: 10
    },
    getters: {
        events(state) {
            return state.events
        },
        event(state) {
            return state.event
        },
    },
    mutations: {
        SET_EVENTS: (state, payload) => {
            state.events = payload
        },
        SET_EVENT: (state, payload) => {
            state.event = payload
        },
        SET_SIGNUP: (state, payload) => {
            state.signup = payload
        },
        SET_SIGNIN: (state, payload) => {
            state.signin = payload
        },
        SET_USER: (state, payload) => {
            state.user = payload
        },
        SET_OFFSET: (state, payload) => {
            state.offset = payload
        },
    },
    actions: {
        clearEvents: ({ commit }) => {
            commit('SET_EVENTS', [])
        },
        clearEvent: ({ commit }) => {
            commit('SET_EVENT', {})
        },
        clearSignup: ({ commit }) => {
            commit('SET_SIGNUP', {})
        },
        clearSignin: ({ commit }) => {
            commit('SET_SIGNIN', {})
        },
        clearUser: ({ commit }) => {
            commit('SET_USER', {})
        },

        getEvents: async ({ commit, state }) => {
            const { offset, limit} = state
            const { data } = await axios.get('http://localhost/api/events?offset=' + offset + '&limit=' + limit)
            console.log('get events', data)
            commit('SET_EVENTS', data)
        },
        getEvent: async ({ commit }, payload) => {
            const { data } = await axios.get('http://localhost/api/event/' + payload)
            console.log('get event', data)
            commit('SET_EVENT', data)
        },

        getPrevPage: ({ commit, dispatch, state }) => {
            commit('SET_OFFSET', state.offset >= state.limit ? state.offset - state.limit : 0)
            dispatch('getEvents')
        },
        getNextPage: ({ commit, dispatch, state }) => {
            commit('SET_OFFSET', state.offset + state.limit)
            dispatch('getEvents')
        },
        getHomePage: ({ commit, dispatch }) => {
            commit('SET_OFFSET', 0)
            dispatch('getEvents')
            router.push('/')

        },
        getCreatePage: ({ commit, dispatch }) => {
            dispatch('clearEvent')
            router.push('/create')
        },
        goBack: () => {
            router.go(-1)
        },
        getSignupPage: ({ commit, dispatch }) => {
            router.push('/signup')

        },
        getSigninPage: ({ commit, dispatch }) => {
            router.push('/signin')
        },

        saveEvent: async function ({ dispatch, state }) {
            const { data } = await axios.post('http://localhost/api/event/save', state.event)
            console.log('save event', data)
            router.push('/event/' + data.id)
        },
        doSignin: async function ({ commit, dispatch, state }) {
            const { data } = await axios.post('http://localhost/api/signin', state.signin)
            console.log('do signin', data)
            commit('SET_USER', data.user)
            router.push('/')
        },
        doSignout: function ({ commit, dispatch, state }) {
            dispatch('clearUser')
            dispatch('getHomePage')
        },
        checkSess: async function ({ commit }) {
            const { data } = await axios.get('http://localhost/api/checkSess')
            console.log('checkSess', data)
            if(data.user) {
                commit('SET_USER', data.user)
            }
        },
    },
})
