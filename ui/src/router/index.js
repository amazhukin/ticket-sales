import Vue from 'vue'
import Router from 'vue-router'
import EventsList from '../components/Event/List.vue'
import EventDetail from '../components/Event/Detail.vue'
import EventEdit from '../components/Event/Edit.vue'
import EventBuy from '../components/Event/Buy.vue'
import Signup from '../components/Signup.vue'
import Signin from '../components/Signin.vue'

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name:'eventsList',
            component: EventsList,
        },
        {
            path: '/create',
            name:'eventCreate',
            component: EventEdit,
        },
        {
            path: '/event/:id',
            name:'eventDetail',
            component: EventDetail,
            props: true,
        },
        {
            path: '/event/:id/edit',
            name:'eventEdit',
            component: EventEdit,
            props: true,
        },
        {
            path: '/event/:id/buy',
            name:'eventBuy',
            component: EventBuy,
            props: true,
        },
        {
            path: '/signup',
            name:'signup',
            component: Signup,
        },
        {
            path: '/signin',
            name:'signin',
            component: Signin,
        },
    ]
})
