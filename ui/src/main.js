import 'babel-polyfill'
import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import VueCookie from 'vue-cookie'
import 'es6-promise/auto'
import { store } from './store'
import router from './router'
import App from './App.vue'

Vue.use(Vuex)
Vue.use(Vuetify)
Vue.use(VueCookie)

new Vue({
 el: '#app',
 render: h => h(App),
 router,
 store
})
