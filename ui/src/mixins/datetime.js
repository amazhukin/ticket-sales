
export default {
    methods: {
        dateFromStampByFormat: function (unix_timestamp, format) {
            const date = new Date(unix_timestamp*1000)
            return date.toLocaleString(format, { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric' })
        }
    }
}
