<?php

namespace App\Misc;

use Symfony\Component\HttpFoundation\JsonResponse;

class UiResponse extends JsonResponse
{
    public function __construct($data = null)
    {
        $headers = [
            'Access-Control-Allow-Origin' => 'http://localhost:8080',
            'Access-Control-Allow-Credentials' => true,
            'Access-Control-Allow-Headers' => 'content-type, access-control-allow-credentials',
        ];
        parent::__construct($data, 200, $headers);
    }
}
