<?php

namespace App\Controller\Api;

use App\Entity\Event;
use App\Misc\UiResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EventController extends AbstractController
{
    /**
     * @Route("/api/events")
     */
    public function list(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Event::class);
        $events = $repository->findByLimitAndOffset(
            $request->get('limit', 10),
            $request->get('offset', 0)
        );

        $res = [];
        foreach ($events as $e) {
            $res[] = $e->prepareData();
        }

        $response = new UiResponse($res);
        return $response;
    }

    /**
     * @Route("/api/event/{id}", requirements={"id"="\d+"})
     */
    public function detail(Event $event)
    {
        $response = new UiResponse($event->prepareData());
        return $response;
    }

    /**
     * @Route("/api/event/save")
     */
    public function save(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if(!empty($data['id'])) {
            //saving

            $details = ['id' => $data['id'], 'message' => 'saved', 'data' => $data];
        } else {
            //creating
            $details = ['id' => rand(1, 50), 'message' => 'created', 'data' => $data];
        }
        $response = new UiResponse($details);
        return $response;
    }

}
