<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Misc\UiResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/api/signup", methods={"OPTIONS"})
     * @Route("/api/signin", methods={"OPTIONS"})
     */
    public function index()
    {
        return new UiResponse();
    }

    /**
     * @Route("/api/signup", methods={"POST"})
     */
    public function signup(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $data = json_decode($request->getContent(), true);

        $user = new User();
        $user->setEmail((string)$data['email']);
        $user->setRoles(['ROLE_BUYER']);
        $user->setFirstName((string)$data['first_name']);
        $user->setLastName((string)$data['last_name']);
        if (!empty($data['middle_name'])) {
            $user->setMiddleName((string)$data['middle_name']);
        }
        $user->setPassword(($encoder->encodePassword($user, (string)$data['password'])));

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $response = new UiResponse(['status' => 1, 'user_id' => $user->getId()]);
        return $response;
    }

    /**
     * @Route("/api/signin", methods={"POST"})
     */
    public function signin(Request $request, UserPasswordEncoderInterface $encoder, SessionInterface $session)
    {
        $res = ['status' => 0];
        $data = json_decode($request->getContent(), true);

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy([
                'email' => (string) $data['email']
            ]);
        if($encoder->isPasswordValid($user, (string)$data['password'])) {
            $session->set('user', $user->prepareData());
            $res = [
                'status' => 1,
                'user' => $user->prepareData(),
                'sessId' => $session->getId()
            ];
        }


        $response = new UiResponse($res);
        return $response;
    }

    /**
     * @Route("/api/checkSess", methods={"GET"})
     */
    public function checkSess(SessionInterface $session)
    {
        $res = [
            'status' => 1,
            'user' => $session->get('user'),
        ];

        $response = new UiResponse($res);
        return $response;
    }
}
