<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/{any}", name="front", requirements={"any"=".*"})
     */
    public function index()
    {
        return new Response('
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>vue-spa</title>
  </head>
  <body>
    <div id="app"></div>
    <script src="/spa/build.js"></script>
  </body>
</html>
        ');
    }

}
