<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelloWorldController
{

    /**
    * @Route("/hello")
    */
    public function number()
    {
        return new Response(
            '<html><body>Hello World!</body></html>'
        );
    }
}
