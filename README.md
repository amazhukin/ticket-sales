**Clone this project**

git clone https://amazhukin@bitbucket.org/amazhukin/ticket-sales.git

---

**Run docker compose**

docker-compose up -d

---
## Configure MySQL DB

**Go to container bash**

docker-compose exec db bash

---

**Change directory to app folder**

mysql -u root -p

---

**Enter root password (dbrootpw)**

**Run next queries**

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'dbrootpw';
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'dbrootpw';
quit;

**Upload test data**

mysql -u root -p symfony4 < /var/lib/mysql/dump.sql

---

**Enter root password (dbrootpw)**

**Exit from container**

exit

---

## Install & configure Symfony

**Go to container bash**

docker-compose exec php-fpm bash

---

**Change directory to app folder**

cd /var/www/sf

---

**Install needed symfony environment**

composer update

---

**Create database structure**

php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate

---

**Exit from container**

exit

---

## Install Vue SPA example

**Go to front folder**

cd ui

---

**Install needed vue environment**

npm install

---

**Build front part**

npm run build

---
